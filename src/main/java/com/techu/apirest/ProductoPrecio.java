package com.techu.apirest;

public class ProductoPrecio {
    private String id;
    private Double precio;

    public ProductoPrecio(String  id, Double precio) {
        this.id = id;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
