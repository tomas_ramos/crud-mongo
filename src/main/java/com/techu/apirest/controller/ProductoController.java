package com.techu.apirest.controller;


import com.techu.apirest.ProductoPrecio;
import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping( "/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping( "/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){

        return productoService.findById(id);

    }

     //añadir producto nuevo
    @PostMapping( "/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProduct){
        ProductoModel productonull = new ProductoModel();
        if(productoService.existsById(newProduct.getId())){
            //Ya existe el producto, no lo puedo insertar...
            return productonull;
        }else{
            //no existe, lo puedo crear:
            productoService.save(newProduct);
            return newProduct;
        }

    }

    //actualizar producto existente. Esta vez no pasamos el id, lo cogeremos del producto directamente
    @PutMapping( "/productos")
    public ProductoModel putProductos(@RequestBody ProductoModel productoToUpdate){
        ProductoModel productonull = new ProductoModel();
        if (productoService.existsById(productoToUpdate.getId()) ){
            // Ya existe el producto, por lo que lo puedo modificar
            productoService.save(productoToUpdate);
            return productoToUpdate; //("Producto actualizado correctamente");
        } else {
            // no existe el producto, no lo puedo actualizar
            return productonull;
        }
    }


    // ahora un delete
    @DeleteMapping ( "/productos")
    public boolean delProductos(@RequestBody ProductoModel productoToDelete){
        if(productoService.existsById(productoToDelete.getId())){
            productoService.delete(productoToDelete);
            return true;
        }else{
            return false;
        }


    }

    @DeleteMapping ( "/productos/{id}")
    public boolean delProductosID(@PathVariable String id){
        if (productoService.existsById(id)){
            productoService.deleteById(id);
            return true;
        }else{
            return false;
        }

    }

    @PatchMapping ("/productos")
    public ProductoModel patchProducto(@RequestBody ProductoPrecio precio){
       Optional<ProductoModel> productonull = productoService.findById(precio.getId());
       ProductoModel productoActualizar;
        if (productoService.existsById(precio.getId())){
            //encuentra el producto, por lo que lo podemos actualizar
            productoActualizar = productonull.get();
            productoActualizar.setPrecio(precio.getPrecio());
            productoService.save(productoActualizar);
            return productoActualizar;
        }else{
            //no encuentra el producto, devolver objeto null
            productoActualizar = new ProductoModel();
            return productoActualizar;
        }
    }

}
