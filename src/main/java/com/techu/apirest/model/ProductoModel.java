package com.techu.apirest.model;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "productos") //para que productoModel sea una colección de la BBDD
public class ProductoModel {

    @Id
    @NotNull
    private String id;
    private String descripcion;
    private Double precio;

    //code -- generate -- constructor


    public ProductoModel(String id, String descripcion, Double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    //creamos tb un constructor vacío por lo que pueda pasar...
    public ProductoModel(){}


    //ahora añado getters and setters


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
